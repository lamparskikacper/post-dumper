# Post-dumper
This tool allows mass dumping of posts from JSONPlaceholder service into a set of local files.

# Usage
```bash
./gradlew bootJar
java -jar build/libs/post-dumper*.jar --targetDirectory=*some directory*
```

`targetDirectory` will be created in case it doesn't already exist. 

Be aware that all files touched by the program will be overwritten if ran again!