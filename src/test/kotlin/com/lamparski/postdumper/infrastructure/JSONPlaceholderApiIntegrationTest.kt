package com.lamparski.postdumper.infrastructure

import assertk.all
import assertk.assertThat
import assertk.assertions.hasSize
import assertk.assertions.index
import assertk.assertions.isEqualTo
import assertk.assertions.prop
import com.lamparski.postdumper.domain.Post
import com.lamparski.postdumper.domain.vo.Body
import com.lamparski.postdumper.domain.vo.PostId
import com.lamparski.postdumper.domain.vo.Title
import com.lamparski.postdumper.domain.vo.UserId
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.web.reactive.function.client.WebClient

@OptIn(ExperimentalCoroutinesApi::class)
internal class JSONPlaceholderApiIntegrationTest {
	private val mockServer = MockWebServer()
	private val api: JSONPlaceholderApi = with(mockServer.url("/").toUrl()) {
		JSONPlaceholderApi(
			this,
			WebClient.builder().baseUrl(this.toString()),
		)
	}

	@AfterEach
	fun stopServer() {
		mockServer.shutdown()
	}

	@Test
	fun `api - should properly deserialize and return Post`() = runTest {
		//given
		mockServer.enqueue(
			MockResponse()
				.setBody("""[{"userId":1,"id":2,"title":"foo","body":"bar"}]""")
				.setHeader("Content-Type", "application/json")
		)

		//when
		val result = api.findAll().toList()

		//then
		assertThat(result).all {
			hasSize(1)
			index(0).all {
				prop(Post::id).isEqualTo(PostId(2))
				prop(Post::userId).isEqualTo(UserId(1))
				prop(Post::title).isEqualTo(Title("foo"))
				prop(Post::body).isEqualTo(Body("bar"))
			}
		}
	}
}