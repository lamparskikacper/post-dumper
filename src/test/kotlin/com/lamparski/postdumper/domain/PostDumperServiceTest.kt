package com.lamparski.postdumper.domain

import assertk.assertThat
import assertk.assertions.contains
import assertk.assertions.doesNotContain
import assertk.assertions.exists
import com.fasterxml.jackson.databind.ObjectMapper
import com.lamparski.postdumper.domain.vo.Body
import com.lamparski.postdumper.domain.vo.PostId
import com.lamparski.postdumper.domain.vo.Title
import com.lamparski.postdumper.domain.vo.UserId
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.Writer
import java.nio.file.Path
import kotlin.io.path.createFile
import kotlin.io.path.readLines
import kotlin.io.path.writeText

@OptIn(ExperimentalCoroutinesApi::class)
internal class PostDumperServiceTest {

	private val postRepository = mockk<PostRepository> {
		coEvery { findAll() } returns emptyFlow()
	}
	private val objectMapper = spyk(ObjectMapper())

	private val postDumperService = PostDumperService(
		postRepository = postRepository,
		objectMapper = objectMapper,
	)

	@TempDir
	private lateinit var tempDir: Path

	@Test
	fun `dumpAllPosts - should create target dir if it doesn't exist`() = runTest {
		//given
		val notExistingDirectory = tempDir.resolve("foo/bar/baz")

		//when
		postDumperService.dumpAllPosts(notExistingDirectory)

		//then
		assertThat(notExistingDirectory).exists()
	}

	@Test
	fun `dumpAllPosts - should name a file with post id`() = runTest {
		//given
		val post = generatePost(PostId(1234))
		coEvery { postRepository.findAll() } returns flowOf(post)

		//when
		postDumperService.dumpAllPosts(tempDir)

		//then
		assertThat(tempDir.resolve("${post.id.value}.json")).exists()
	}

	@Test
	fun `dumpAllPosts - should overwrite existing file`() = runTest {
		//given
		val existingFile = tempDir
			.resolve("1234.json")
			.createFile()
			.also {
				it.writeText("foobar")
			}
		coEvery { postRepository.findAll() } returns flowOf(generatePost(PostId(1234)))

		//when
		postDumperService.dumpAllPosts(tempDir)

		//then
		assertThat(existingFile.readLines()).doesNotContain("foobar")
	}

	@Test
	fun `dumpAllPosts - should insert objectMapper answer into file content`() = runTest {
		//given
		val post = generatePost(PostId(1234))
		coEvery { postRepository.findAll() } returns flowOf(post)
		every { objectMapper.writeValue(any<Writer>(), post) } answers {
			arg<Writer>(0).write("Some random content")
		}

		//when
		postDumperService.dumpAllPosts(tempDir)

		//then
		verify { objectMapper.writeValue(any<Writer>(), post) }
		assertThat(
			tempDir.resolve("1234.json").readLines()
		).contains("Some random content")
	}

	private fun generatePost(id: PostId) = Post(
		id = id,
		userId = UserId(1),
		title = Title("Foo"),
		body = Body("")
	)
}