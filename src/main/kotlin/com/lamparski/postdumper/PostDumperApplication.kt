package com.lamparski.postdumper

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
internal class PostDumperApplication

internal fun main(args: Array<String>) {
	runApplication<PostDumperApplication>(*args)
}
