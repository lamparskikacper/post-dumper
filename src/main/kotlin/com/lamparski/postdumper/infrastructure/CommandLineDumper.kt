package com.lamparski.postdumper.infrastructure

import com.lamparski.postdumper.domain.PostDumperService
import kotlinx.coroutines.runBlocking
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import java.nio.file.Paths

@Component
@Profile("!no-cli")
internal class CommandLineDumper(
	private val postDumper: PostDumperService,
): ApplicationRunner {

	companion object {
		const val ARG_NAME = "targetDirectory"
	}

	override fun run(args: ApplicationArguments) = runBlocking {
		val targetDirectory = args.getOptionValues(ARG_NAME)
			?.firstOrNull()
			?.let { Paths.get(it) }
			?: throw IllegalArgumentException("You must specify target directory with --$ARG_NAME")

		postDumper.dumpAllPosts(targetDirectory)
	}
}