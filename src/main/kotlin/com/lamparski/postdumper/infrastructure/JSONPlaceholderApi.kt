package com.lamparski.postdumper.infrastructure

import com.lamparski.postdumper.domain.Post
import com.lamparski.postdumper.domain.PostRepository
import kotlinx.coroutines.flow.Flow
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToFlow
import java.net.URL

@Component
internal class JSONPlaceholderApi(
	@Value("\${post-dumper.json-placeholder.base-url}")
	baseUrl: URL,
	webClientBuilder: WebClient.Builder
): PostRepository {

	private val webClient = webClientBuilder
		.baseUrl(baseUrl.toString())
		.build()

	override suspend fun findAll(): Flow<Post> {
		return webClient.get()
			.uri { it.path("/posts").build() }
			.retrieve()
			.bodyToFlow()
	}

}