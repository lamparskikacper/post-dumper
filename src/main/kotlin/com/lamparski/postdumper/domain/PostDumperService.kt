package com.lamparski.postdumper.domain

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Component
import java.nio.file.Path
import kotlin.io.path.createDirectories
import kotlin.io.path.createFile
import kotlin.io.path.exists
import kotlin.io.path.writer

@Component
internal class PostDumperService(
	private val postRepository: PostRepository,
	private val objectMapper: ObjectMapper,
) {

	suspend fun dumpAllPosts(targetDirectory: Path) {
		createTargetIfNotExists(targetDirectory)

		postRepository.findAll()
			.collect { savePost(targetDirectory, it) }
	}

	private fun createTargetIfNotExists(targetDirectory: Path) {
		targetDirectory.createDirectories()
	}

	private fun savePost(targetDirectory: Path, post: Post) {
		val path = targetDirectory.resolve("${post.id.value}.json")

		if (!path.exists()) {
			path.createFile()
		}

		path.writer().use { objectMapper.writeValue(it, post) }
	}
}