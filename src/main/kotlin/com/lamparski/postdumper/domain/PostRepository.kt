package com.lamparski.postdumper.domain

import kotlinx.coroutines.flow.Flow

internal interface PostRepository {
	suspend fun findAll(): Flow<Post>
}