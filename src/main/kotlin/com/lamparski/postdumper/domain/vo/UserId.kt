package com.lamparski.postdumper.domain.vo

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue

internal data class UserId @JsonCreator(mode = JsonCreator.Mode.DELEGATING) constructor(
	@JsonValue val value: Int,
)
