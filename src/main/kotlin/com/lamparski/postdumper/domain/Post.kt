package com.lamparski.postdumper.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.lamparski.postdumper.domain.vo.Body
import com.lamparski.postdumper.domain.vo.PostId
import com.lamparski.postdumper.domain.vo.Title
import com.lamparski.postdumper.domain.vo.UserId

@JsonIgnoreProperties(ignoreUnknown = true)
internal data class Post(
	val id: PostId,
	val userId: UserId,
	val title: Title,
	val body: Body,
)
